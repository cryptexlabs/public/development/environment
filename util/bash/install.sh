#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Colors
green='\033[0;32m'
yellow='\033[0;93m'
red='\033[0;31m'
no_color='\033[0m'

# Inputs
if [[ -z "${1}" ]]; then
    echo -e "${red}Missing argument 1 for operating system${no_color}"
fi

operating_system=$1

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname $(dirname ${SCRIPT_DIRECTORY}))
OS_DIRECTORY="${PROJECT_DIRECTORY}/${operating_system}"

## Apps
${PROJECT_DIRECTORY}/apps/install.sh ${operating_system}

echo ""
echo "**************************************************************************************"
echo ""
echo -e "${green}Success! Dev environment installation is complete. Happy Coding!${no_color}"
echo ""
echo "**************************************************************************************"
echo ""

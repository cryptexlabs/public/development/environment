#!/usr/bin/env bash

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
PROJECTS_PATH=~/Projects/CryptexLabs

install_project() {

    project_parent_directory=$1
    project_name=$2
    operating_system=$3

    project_directory="${project_parent_directory}/${project_name}"
    project_path="${PROJECTS_PATH}/${project_directory}"

    # Clone and configure project
    if [[ ! -d "${project_path}" ]]; then
        mkdir -p $(dirname ${project_path})
        git clone git@gitlab.com:cryptexlabs/private/$(echo ${project_directory} | tr '[:upper:]' '[:lower:]').git "${project_path}"
    fi
    "${project_path}/bin/${operating_system}/install.sh"
}
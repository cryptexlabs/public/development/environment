#!/usr/bin/env bash

set -e

PROJECTS_PATH=~/Projects/CryptexLabs

# Download the dev environment repository
rm -f ~/Downloads/development.zip
wget -O ~/Downloads/development.zip https://gitlab.com/api/v4/projects/11723449/repository/archive.zip
unzip -qq ~/Downloads/development.zip -d ~/Downloads/development-master
find ~/Downloads/development-master -name "environment-master-*" -type d -exec cp -Rf {} ~/Downloads/development \;
find ~/Downloads/development -type f -iname "*.sh" -exec chmod +x {} \;
mkdir -p ${PROJECTS_PATH}/Development/
rm -rf ${PROJECTS_PATH}/Development/environment
mv ~/Downloads/development ${PROJECTS_PATH}/Development/environment
rm -r ~/Downloads/development-master/
rm ~/Downloads/development.zip

${PROJECTS_PATH}/Development/environment/ubuntu/install.sh
#!/usr/bin/env bash

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
OS_BIN_DIRECTORY=$(basename $(dirname $(realpath $0)))
PROJECTS_PATH=$(dirname $(dirname $(dirname $(realpath $0))))

get_github_latest_release() {
    curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                              # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                      # Pluck JSON value
}

install_project() {

    project_parent_directory=$1
    project_name=$2

    project_directory="${project_parent_directory}/${project_name}"
    project_path="${PROJECTS_PATH}/${project_directory}"

    # Clone and configure project
    if [[ ! -d "${project_path}" ]]; then
        mkdir -p $(dirname ${project_path})
        git clone git@gitlab.com:cryptexlabs/private/$(echo ${project_directory} | tr '[:upper:]' '[:lower:]').git "${project_path}"
    fi
    "${project_path}/bin/${OS_BIN_DIRECTORY}/install.sh"
}

################################################################################
# Executes command with a timeout
# Params:
#   $1 timeout in seconds
#   $2 command
# Returns 1 if timed out 0 otherwise
timeout() {

    time=$1

    # start the command in a subshell to avoid problem with pipes
    # (spawn accepts one command)
    command="/bin/sh -c \"$2\""

    expect -c "set echo \"-noecho\"; set timeout $time; spawn -noecho $command; expect timeout { exit 1 } eof { exit 0 }"

    if [[ $? = 1 ]] ; then
        echo "Timeout after ${time} seconds"
    fi

}
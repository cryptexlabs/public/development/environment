#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname ${SCRIPT_DIRECTORY})
OPERATING_SYSTEM=$(basename ${SCRIPT_DIRECTORY})

# kubedev installs local development tools
wget https://gitlab.com/cryptexlabs/public/development/kubedev/raw/master/ubuntu/install-remote.sh \
    -O /tmp/install-remote.sh
chmod +x /tmp/install-remote.sh
/tmp/install-remote.sh "false"
rm /tmp/install-remote.sh

# All other things
${PROJECT_DIRECTORY}/util/bash/install.sh "${OPERATING_SYSTEM}"

/usr/local/kubedev/dist/macos/sudo/disable-passwordless-sudo.sh
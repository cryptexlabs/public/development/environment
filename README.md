# Development Environment Setup

## Mac OS

To set up your development environment for the first time on Mac OS please run the following command:

```bash
bash <(curl -s https://gitlab.com/cryptexlabs/public/development/environment/raw/master/macos/install-remote.sh)
```

## Ubuntu

To set up your development environment for the first time on Mac OS please run the following command:

```bash
bash <(wget -o /dev/null -O - https://gitlab.com/cryptexlabs/public/development/environment/raw/master/ubuntu/install-remote.sh)
```
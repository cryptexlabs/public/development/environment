#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

operating_system=$1

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

# Ops
${SCRIPT_DIRECTORY}/ops/install.sh ${operating_system}

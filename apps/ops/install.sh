#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

operating_system=$1

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))

${SCRIPT_DIRECTORY}/reverse-proxy.sh ${operating_system}
#${SCRIPT_DIRECTORY}/container-cluster-orchestrator.sh ${operating_system}
#${SCRIPT_DIRECTORY}/container-image-registry.sh ${operating_system}
#${SCRIPT_DIRECTORY}/jenkins.sh ${operating_system}
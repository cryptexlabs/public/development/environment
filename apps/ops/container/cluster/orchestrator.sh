#!/usr/bin/env bash

set -e

# Path function
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

# Variables
SCRIPT_DIRECTORY=$(dirname $(realpath $0))
PROJECT_DIRECTORY=$(dirname $(dirname $(dirname $(dirname ${SCRIPT_DIRECTORY}))))

# Helper functions
source "${PROJECT_DIRECTORY}/util/bash/fn.sh"

script_directory_parent_parent_name=$(basename $(dirname $(dirname ${SCRIPT_DIRECTORY})))
script_directory_parent_name=$(basename $(dirname ${SCRIPT_DIRECTORY}))
script_directory_name=$(basename ${SCRIPT_DIRECTORY})
project_parent_directory="${script_directory_parent_parent_name^}/${script_directory_parent_name^}/${script_directory_name^}"
project_name=$(basename $(realpath $0) .sh)

install_project "${project_parent_directory}" "${project_name}"